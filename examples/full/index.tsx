import { Node } from "@stembord/nodes";
import { InternalQuiz } from "@stembord/react-quiz";
import "katex/dist/katex.min.css";
import { render } from "react-dom";
import stembord from "../../stembord.json";

fetch("index.js")
  .then(response => response.text())
  .then(text => {
    const body = text.replace(/@PUBLIC_URL@/g, "");

    eval(body);

    const {
      data: { quizData }
    } = Node.get(stembord.name);

    render(<InternalQuiz quiz={quizData} />, document.getElementById("app"));
  });
