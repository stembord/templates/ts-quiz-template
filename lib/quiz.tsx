import {
  Choice,
  Explanation,
  InputPrompt,
  InputType,
  MultipleChoicePrompt,
  Question,
  Quiz,
  TrueFalsePrompt
} from "@stembord/react-quiz";
import * as React from "react";
import { BlockMath } from "react-katex";

export default (
  <Quiz title="Mathematical Properties">
    <Question>
      <InputPrompt>
        {Input => (
          <p>
            Fill in the{" "}
            <Input type={InputType.String} answer={["blank", "Blank"]} />
          </p>
        )}
      </InputPrompt>
      <Explanation>
        <p>It is a fill in the blank question that says fill in the _____</p>
      </Explanation>
    </Question>
    <Question>
      <TrueFalsePrompt answer={true} true="Yes" false="No">
        <p>Is the square root of 2 a real number?</p>
      </TrueFalsePrompt>
      <Explanation>
        <p>Yes, google it</p>
      </Explanation>
    </Question>
    <Question>
      <MultipleChoicePrompt
        onlyOne
        choices={[
          <Choice answer>
            <BlockMath math="\left(\prod _{i=1}^{n}x_{i}\right)^{\frac {1}{n}}={\sqrt[{n}]{x_{1}x_{2}\cdots x_{n}}}" />
          </Choice>,
          <Choice>
            <BlockMath math="\bar{x}= \frac{1}{n}\cdot \sum_{i=1}^{n} x_{i}" />
          </Choice>,
          <Choice>
            <BlockMath math="\frac{1}{\sqrt{2\cdot x}}" />
          </Choice>
        ]}
      >
        <p>
          Which of the following formulas gives the geometric mean of a series
          of numbers?
        </p>
      </MultipleChoicePrompt>
      <Explanation>
        <p>I do not want to explain this one</p>
      </Explanation>
    </Question>
  </Quiz>
);
