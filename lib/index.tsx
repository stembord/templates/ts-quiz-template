import { Node } from "@stembord/nodes";
import { Quiz } from "@stembord/react-quiz";
import stembord from "../stembord.json";
import quiz from "./quiz";

Node.create(stembord, {
  quizData: Quiz.parse(quiz.props)
});
